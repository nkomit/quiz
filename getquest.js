let quizRus;

if (window.location.hash === "#rus"){
  //функция рус
  console.log('russian');
  makeRus();
}


//перевести содержимое стр на русский
async function makeRus () {
  await fetch('russian.json')
    .then((response) => {
      console.log('processing...');
      return response.json();
    })
    .then((data) => {
      quizRus = data;
    })
    .then(() => {
      console.log('translating...');
      //текст
      document.getElementById('quiz_name').textContent = quizRus.quiz_name;
      document.getElementById('start').value = quizRus.startbtn;
      document.querySelector('.paragraph').textContent = quizRus.paragraph;
      document.querySelector('.subparagraph').textContent = quizRus.subparagraph;
      //кнопки
      buttons.forEach((elem) => {
        if (elem.dataset.type == 'next') {
          elem.textContent = quizRus.nextBtn;
        }
        else if (elem.dataset.type == 'submit') {
          elem.textContent = quizRus.finishBtn;
        }
        else if (elem.dataset.type == 'reset') {
          elem.textContent = quizRus.backBtn;
        }
      })
      // варианты ответов и комментарии к ним
      for (i=0; i<quizRus.questions.length; i++) {
       questions[i].textContent = quizRus.questions[i].title;
       for (j=0; j<quizRus.questions[i].answers.length; j++) {
         let qId = quizRus.questions[i].answers[j].id;
         for (k=0; k<answers.length; k++) {
           let labelTxt = answers[k].labels[0];
           if (labelTxt.htmlFor === qId) {
             labelTxt.textContent = quizRus.questions[i].answers[j].text;
             labelTxt.nextElementSibling.textContent = quizRus.questions[i].answers[j].comment;
           }
         }
       }
      }

    })
}


// перезагружает страницу при выборе языка
function reloadPage() {
  console.log('page reloaded');
  setTimeout(function () {
    location.reload();
  }, 100);
}
