const startBtn = document.getElementById('start');   //стартовая кнопка, запускает квиз
const firstQuizcard = document.getElementById('firstQuizcard');  //первая карточка
const quiz = document.getElementById('quiz-body');
const buttons = Array.from(document.getElementsByClassName('quiz__card-button'));  //кнопки "след вопрос"
const questions = Array.from(document.querySelectorAll('.quiz__card-question')); //вопросы
const answers = Array.from(document.querySelectorAll('ul.quiz__card-answers > li > input')); //варианты ответа

let score = 0;  //очки


startBtn.addEventListener('click', startQuiz);

//появляется первая карточка
function startQuiz() {
  console.log('quiz started');
  startBtn.setAttribute('style', 'display:none');
  firstQuizcard.classList.add('visible');
}

//при выборе варианта ответа появляется кнопка "след вопрос"
answers.forEach((answer) =>{
  let quizCardBody = answer.closest('.quiz__card');
  let nextButton = quizCardBody.querySelector('.quiz__card-button');
  let small = answer.parentElement.querySelector('small');  //комментарий к выбранному ответу

  answer.addEventListener('click', function(event){
    //появляется кнопка "след вопрос"
    nextButton.classList.add('visible');

    //если ответ правильный, +1 балл, если нет - баллы без изм, показать прав ответ
    if (answer.parentElement.classList.contains('correct')) {
      score++;
      console.log(score);
    }
    else {
      quizCardBody.querySelector('.correct').setAttribute('style', 'color:green');
    }

    quizCardBody.classList.add('answered');          //карточка отмечается "отвеченной"
    answer.parentElement.classList.add('checked');   //ответ отмечается "отмеченным"
  })
})


//показать результат и комментарий
function showResult() {
  document.querySelector('.paragraph').remove();
  document.querySelector('.subparagraph').remove();
  if (score <= 1) {
    if (window.location.hash === "#rus") {
      document.getElementById("result").innerHTML = "Вы набрали " + score + ". В другой раз повезет!";
    }
    else {
      document.getElementById("result").innerHTML = "You scored " + score + ". Maybe next time!";
    }
  }
  else if (score == 2) {
    if (window.location.hash === "#rus") {
      document.getElementById("result").innerHTML = "Вы набрали " + score + ". Неплохо!";
    }
    else {
      document.getElementById("result").innerHTML = "You scored " + score + ". Not bad!";
    }
  }
  else {
    if (window.location.hash === "#rus") {
      document.getElementById("result").innerHTML = "Вы набрали " + score + ". Поздравляем!";
    }
    else {
      document.getElementById("result").innerHTML = "You scored " + score + ". Congrats!";
    }
  }
}

//при клике по кнопке Next появляется следующая карточка,
//а текущая карточка и сама кнопка исчезают

buttons.forEach((elem) =>{
  elem.addEventListener('click', function(event){
    elem.parentElement.classList.remove('visible');
    if (elem.value == 'Finish') {
      showResult();
      quiz.querySelector('button[value=Back]').classList.add('visible');
    }
    else if (elem.value == 'Back') {
      //reset quiz
      location.reload();
    }
    else {
      quiz.querySelector('.quiz__card:not(.answered)').classList.add('visible'); //показывает первую неотвеченную карточку
    }
  })
})
